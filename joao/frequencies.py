# -*-coding:utf-8 -*c

import numpy as np
import scipy.constants as cst
import matplotlib.pyplot as plt
import scipy.integrate as inte
import scipy.special as spe 

CMB = np.array( [244.1, 371.74, 483.69, 287.45, 58.04, 2.27])

 ''' Coefficients given by the ILC'''
 
SZ3comp = np.array([ -1.22751619e+17,   1.45317279e+16,   5.01026613e+16,   
                    2.45053373e+15,  -6.53477518e+15,   1.04048565e+15])
                    # 3 compo
SZ2comp1 = np.array([ -7.57843407e+16,  -1.85950882e+16,   4.56669999e+16,   
                    1.37290761e+16,  -1.08016184e+16,   1.46585541e+15])
                    # sz +CMB
SZ2comp2 = np.array( [ -1.01331163e+17, -1.27340883e+15, 3.79019437e+16,
                      6.27703590e+15, -6.57426455e+15, 9.83708591e+14])
                     #sz +dust
SZ1comp = np.array([ -4.70528969e+16,  -3.97261577e+16,   3.03023856e+16,   
                    1.89910691e+16,  -1.10419650e+16,   1.41434726e+15])
                    #sz 
CMB1comp = np.array([ -3.75561584e-03, 3.23794369e-03, 1.97547552e-03,
                     -8.64543564e-04,1.04617015e-04, -1.78055673e-06])
                     #CMB alone
CMB2comp1 = np.array([ -4.24071877e-03, 3.11891473e-03, 2.26779443e-03,
                       -7.76662414e-04, 3.54748071e-05,   7.60252611e-06])
                       #CMB with SZ
CMB3comp = np.array([ -3.92997977e-03, 2.89974525e-03, 2.23844776e-03, 
                    -7.02042742e-04, 7.24505626e-06, 1.04168039e-05])    
                    #CMB 3 compo
                    
CMB2comp2 = np.array([ -3.39092621e-03, 2.83593038e-03, 2.01842608e-03,
                      -7.12804058e-04, 3.59419799e-05, 5.84759744e-06])
                      #cmb with dust
                      
dust1comp = np.array([-77.01366541, 80.64804253, 2.31469094, -28.75659044,
                      11.66497365, -1.24936576])
                      #dust alone
                      
dust2comp1 = np.array([-113.19749183, 80.19332749, 15.84890211,
                       -26.5151558, 9.31740315, -0.89809829])    
                       #dust with sz
                       
dust2comp2 = np.array([-61.04654822, 67.29427725, 
                       -7.18962846, -25.40015469, 11.49573086, -1.27690079])  
                       #dust with cmb
dust3comp = np.array([-102.3410126, 72.18284755,  9.66524137,
                      -24.57577878, 9.29738885, -0.92687447]) 
                    # dust all                      


def diffspectra (freq, T):
    '''SZ - blackbody CMB'''
    kB=1.38066E-23; # Boltzmann’s constant in J/K
    c=3.0E8
    h = 6.63E-34 #Planck’s constant (JS)
    I0 = 2*(kB*T)**3/(h*c)**2
    
    f1 = freq*10**9*h/(kB * T)
    f2 = f1* 1/(np.tanh(f1/2)) - 4
    totalwI = f2 * f1**4 * np.exp(f1) /(np.exp(f1)-1)**2
    total = totalwI*I0
    
    return total 
    
def greybody (freq, T,beta):
    ''' greybody for the dust'''
    
    kB=1.38066E-23; # Boltzmann’s constant in J/K
    c=3.0E8
    h = 6.63E-34 #Planck’s constant (JS)
    
    f1 = freq*10**9*h/(kB * T)
    f2 = f1**(3+beta) * 1/(np.exp(f1)-1)
    wtot = f2
    
    return wtot
    
def synchrotron (freq):
    
    '''synchrotron'''
    beta = -3
    denom = 0.408
    total =(10**6)*(freq/denom)**beta
    
    return total
    
    

def main():
    spectralresol = 3
    T = 2.715
    Tdustcold = 9.75
    betacold = 1.63
    Tdusthot = 15.70
    betahot = 2.82
    Tcmb = 19.74
    betacmb = 1.6
    #freq = np.arange(1,1000,0.1)

    freq = np.array([100, 143, 217, 353, 545, 857])
    freqmin = freq - freq/spectralresol 
    freqmax = freq + freq/spectralresol
    
    valsz = diffspectra(freq,T)
    realvalsz  = list()
    realvaldust = list()
    realvalsc = list()
    valdust = greybody(freq, Tcmb, betacmb)
    valsc = synchrotron(freq)
    ''' integrated between HFI spectrum resol'''
    for i in range(freqmin.size)  : 
        
        realvalsz.append(inte.quad(lambda x: diffspectra(x,T), freqmin[i],freqmax[i])[0])
        realvaldust.append(inte.quad(lambda x: greybody(x, Tcmb, betacmb), freqmin[i], freqmax[i])[0])
        realvalsc.append(inte.quad(lambda x: synchrotron(x), freqmin[i], freqmax[i])[0])
        
        realvalsz[i] = realvalsz[i]/(freqmax[i]-freqmin[i])
        realvaldust[i] = realvaldust[i]/(freqmax[i]-freqmin[i])
        realvalsc[i] = realvalsc[i]/(freqmax[i]-freqmin[i])
        
    sumsz1 =np.sum(np.array(realvalsz*SZ1comp))
    sumsz21 = np.sum(np.array(realvalsz*SZ2comp1))
    sumsz22 = np.sum(np.array(realvalsz*SZ2comp2))
    sumsz3 = np.sum(np.array(realvalsz*SZ3comp))
    
    sumcmb = np.sum(np.array(CMB*CMB1comp))
    sumcmb21 = np.sum(np.array(CMB*CMB2comp1))
    sumcmb22 = np.sum(np.array(CMB*CMB2comp2))
    sumcmb3 = np.sum(np.array(CMB*CMB3comp))
    print('cmb1')
    print(sumcmb)
    print('cmb21')
    print(sumcmb21)
    print('cmb22')
    print(sumcmb22)
    print('cmb3')
    print(sumcmb3)
    print('1sz')
    print(sumsz1)
    print('2sz')
    print(sumsz21)
    print('2szbis')
    print(sumsz22)
    print('3sz')
    print(sumsz3)
    plt.plot(freq,SZ1comp,'+-' ,color='b', label='SZ no condition')#realvalsz*
    plt.plot(freq, SZ2comp1,'+-', color='r', label='SZ without CMB')#realvalsz
    plt.plot(freq, SZ2comp2,'+-', color='y', label='SZ without Dust')
    plt.plot(freq, SZ3comp,'+-', color = 'k', label='SZ without all')
    plt.axvline(100, color='k')#, label='100 GHZ')
    plt.axvline(143, color='k')#, label='143 GHZ')
    plt.axvline(217, color='k')#, label='217 GHZ')
    plt.axvline(353, color='k')#, label='353 GHZ')
    plt.axvline(545, color='k')#, label='545 GHZ')
    plt.axvline(857, color='k')#, label='857 GHZ')
    plt.legend(loc='upper right')
    plt.title('SZ coefficients values')
    plt.xlabel('frequencies')
    plt.ylabel('coefficients')
    plt.savefig('szcoef.png')
    plt.show()
    plt.figure()
    
    plt.plot(freq, realvalsz*SZ1comp,'+-' , color='b', label='SZ no condition')#realvalsz*
    plt.plot(freq, realvalsz*SZ2comp1,'+-', color='r', label='SZ without CMB')#realvalsz
    plt.plot(freq, realvalsz* SZ2comp2,'+-', color='y', label='SZ without Dust')
    plt.plot(freq, realvalsz*SZ3comp,'+-', color = 'k', label='SZ without all')
    plt.axvline(100, color='k')#, label='100 GHZ')
    plt.axvline(143, color='k')#, label='143 GHZ')
    plt.axvline(217, color='k')#, label='217 GHZ')
    plt.axvline(353, color='k')#, label='353 GHZ')
    plt.axvline(545, color='k')#, label='545 GHZ')
    plt.axvline(857, color='k')#, label='857 GHZ')
    plt.legend(loc='upper right')
    plt.title('SZ*f coefficients values')
    plt.xlabel('frequencies')
    plt.ylabel('coefficients')
    plt.savefig('szcoefxfreq.png')
    plt.show()
    plt.figure()
    
    
    plt.plot(freq,CMB*CMB1comp, color='b', label='CMB alone')
    plt.plot(freq, CMB*CMB2comp1, color='r', label='CMB without SZ')
    plt.plot(freq,CMB*CMB2comp2, color='y', label='CMB without Dust')
    plt.plot(freq, CMB*CMB3comp, color = 'k', label='CMB without all')
    plt.title('CMB coefficients values')
    plt.legend(loc='upper right')
    plt.xlabel('frequencies')
    plt.ylabel('coefficients')
    plt.savefig('cmbcoef.png')
    plt.show()
    plt.figure()
    
    plt.plot(freq,realvaldust*dust1comp, color='b', label='Dust alone')
    plt.plot(freq, realvaldust*dust2comp1, color='r', label='Dust without CMB')
    plt.plot(freq, realvaldust*dust2comp2, color='y', label='Dust without SZ')
    plt.plot(freq, realvaldust*dust3comp, color = 'k', label='Dust without all')
    plt.title('CMB coefficients values')
    plt.legend(loc='upper right')
    plt.xlabel('frequencies')
    plt.ylabel('coefficients')
    plt.savefig('dustcoef.png')
    plt.show()
    
    plt.figure()
    plt.plot(freq, np.array(realvalsz)*10**17, label='Spectrum')
    plt.title('Spectrum of the difference SZ - CMB')
    plt.legend(loc='upper right')
    plt.xlabel('frequencies')
    plt.ylabel('Values of the spectrum')
   # plt.savefig('szgraph.png')
    plt.show()
   
    plt.figure()
    plt.loglog(freq, realvaldust)
    plt.loglog(freq, valdust)
    plt.show()
    
    plt.figure()
    plt.loglog(freq, realvalsc)
    plt.loglog(freq, valsc)
    plt.show()

if __name__ == '__main__':
    main()

