# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 17:15:10 2016

@author: DaRocha
"""

import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import scipy.optimize as sci

FWHM = 9.65/60 #en degre
NSIDE = 2**11
NUMBERPIX = 12*NSIDE**2
NUMBERCLUS = 20000
NUMBERNEIGHMAX = 15 
MEANNOISE = 90
SIGMA = 5
BIN = 1000

class Clusters(object):
    def __init__(self, center, neighboor, lum):
        self.center = center
        self.neighboor = neighboor
        self.lum = lum
        self.listneighclus = list()
        self.totlum = 0
        self.calcullum = 0
        self.error = 0
        
    def zoomclus(self, map):
        '''zoom on cluster'''
        centerang = hp.pix2ang(NSIDE, self.center)
        print(centerang)
        theta = np.rad2deg(centerang[0])
        phi = np.rad2deg(centerang[1])
        alpha = 0
        delta = 90 - theta
        if phi >=180:
            alpha = -(360 - phi)
        else :
            alpha = phi
        hp.gnomview(map, rot=[alpha, delta])
        plt.show()
        
    def creationneigh(self):
        ''' create a cluster of a random form'''
        mapdone = np.zeros(NUMBERPIX)
        creationneighb(self.center, self.neighboor,self, mapdone)
        self.listneighclus = list(set(self.listneighclus))
        self.totlum = (self.lum - MEANNOISE) * (len(self.listneighclus))
    def creationneighgaus(self):
        '''create a circle cluster'''
        coormil = hp.pix2vec(NSIDE, self.center)
        self.listneighclus = hp.query_disc(NSIDE, coormil, np.radians(FWHM))
        self.listneighclus = self.listneighclus[:50]
        self.totlum = (self.lum - MEANNOISE)* (len(self.listneighclus))
        
    def lumclus(self, map):
        
        '''algo flux '''
        coormil = hp.pix2vec(NSIDE, self.center)
        numbcl = 0
        numbrg = 0
        integralclus = 0
        integralring = 0
        pixelsclu = hp.query_disc(NSIDE, coormil, np.radians(FWHM*1.5))
        for item in pixelsclu:
            numbcl +=1
            integralclus += map[item]
        pixelsring1 = hp.query_disc(NSIDE, coormil, np.radians(FWHM*2.5))
        pixelsring2 = hp.query_disc(NSIDE, coormil, np.radians(FWHM*3.5))
        for item in pixelsring2:
            if not(item in pixelsring1):
                numbrg +=1
                integralring += map[item]
        self.calcullum = integralclus - (numbcl/numbrg)*integralring
        self.error = np.abs(self.calcullum - self.totlum)
        
        
                    
def creationneighb(pix, neighnum, cluster, donetab):
    
    ''' create the neighboorhood pixels of the center of the cluster'''
    if donetab[pix] == 1:
        print('done')
    elif neighnum ==0:
        donetab[pix] = 1
        cluster.listneighclus.append(pix)
    else:
        angval = hp.pix2ang(NSIDE, pix)
        listneigh = hp.get_all_neighbours(NSIDE, angval[0], angval[1])
        cluster.listneighclus.append(pix)
        
        for item in listneigh:
            creationneighb(item, neighnum-1, cluster, donetab)
        
    
    
def mapcreation (listcluster):
    
    map = np.zeros(NUMBERPIX)
    #map.fill(90)
    map = np.random.normal(MEANNOISE, SIGMA, NUMBERPIX)
    for item in listcluster:
        for items in item.listneighclus:
            map[items]=item.lum
    #hp.mollview(map, title='testmap')
    #for item in listcluster:
        #item.zoomclus(map)
   # hp.mollzoom(map, title='ezzz')
    plt.show()
    return map

def creationclus(numberofclus, pixels):
    '''cree le nombre de cluster'''
    listclus = list()
    for i in range(numberofclus):
        center = np.random.randint(0, pixels)
        neighboor = np.random.randint(5, NUMBERNEIGHMAX)
        lum = 100
        listclus.append(Clusters(center, neighboor, lum))
        
    return listclus

def erreur(clust):
    '''calculate the mean error of the flux algo'''
    toterror = 0
    for item in clust:
        #print (item.error)
        toterror+= item.error
    return toterror/len(clust)
    
def modelling_function(xgauss, pmax, pmean, pstd):
    """
    compute a gaussian function:
    """
    ygauss = np.exp(-((xgauss-pmean)**2)/(2*pstd**2))*pmax
    return ygauss
    
    
def histolum(clust):
    '''flux fonction mesure'''
    
    listtrue = list()
    listcalc = list()
    lumrel = 0
    tot = 0
    numbrecoup = 0
    for i, item in enumerate(clust):
        listtrue.append(item.totlum)
        listcalc.append(item.calcullum)
        tot = i
        lumrel += item.totlum
        if listtrue[i] != listcalc[i]:
            numbrecoup += 1
    print(numbrecoup)
    bin_values, bin_boundaries = np.histogram(listcalc, BIN)
    
    maxy = np.float(np.max(bin_values))
    normal_y = bin_values/maxy
    maxx = np.float(np.max(bin_boundaries))
    normal_x = bin_boundaries[:-1]/maxx
    
    fit, covariant = sci.curve_fit(modelling_function, normal_x, normal_y)
    
    
    maxvalue = fit[0] * maxy
    background = fit[1] * maxx
    dispersion = fit[2] * maxx
    
    x = normal_x * maxx
    y = normal_y * maxy
    f = plt.figure()
    ax = f.add_subplot(111)
    variance = np.var(listcalc)
    ecart = np.sqrt(variance)
    mean = np.mean(listcalc)
    plt.plot(x, y, 'b+:', label='data')
    plt.plot(bin_boundaries[:-1], modelling_function(bin_boundaries[:-1],\
                maxvalue, background, dispersion), 'r.:', label='fit')
    plt.axvline(lumrel/tot, color='k', label='Theoretical')
    plt.text(0.15, 0.95,'Mean :%4.f' %(mean), horizontalalignment='center',verticalalignment='center', transform = ax.transAxes)
    plt.text(0.15, 0.85,' Standard deviation: %4.f '%(ecart), horizontalalignment='center',verticalalignment='center', transform = ax.transAxes)    
    plt.title('Flux of Clusters Measured')
    plt.xlabel('Flux')
    plt.ylabel('Number of CLusters')
    plt.legend(loc='upper right')
    plt.savefig('fluxtest15sigma5test.png')
    plt.show()
    
    
def main():
    
    listclust = creationclus(NUMBERCLUS, NUMBERPIX)
    for item in listclust:
        item.creationneighgaus()
    map = mapcreation(listclust)
    
    for item in listclust:
        item.lumclus(map)
    histolum(listclust)
    errmoy = erreur(listclust)
    print (errmoy)
    #coordvec = hp.pix2vec(NSIDE, milieu)
    #lumclus(coordvec, map, milieu)
    #createcircle(map, pixelsclu, pixelsring1, pixelsring2,  milieu)
    
    
    
    




if __name__ == '__main__':
    main()