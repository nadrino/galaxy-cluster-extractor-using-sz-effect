# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 10:39:44 2016

@author: DaRocha
"""
FILE = 'clusters.txt'
CATALOG = 'PSZ2v1.fits'
import numpy as np
import scipy.constants as cst
import matplotlib.pyplot as plt
import scipy.integrate as inte
import scipy.special as spe
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.coordinates import match_coordinates_sky

def opentxt():
    '''open our catalog'''
    myfile = open(FILE, 'r')
    contain = myfile.read().split('\n')
    clusters = list()
    for item in contain:
        clusters.append(item.split(' '))
    clusters = clusters[:-1]
    for i, item in enumerate(clusters):
        for j in range(len(item)):
            clusters[i][j] = float(clusters[i][j])
    return clusters
    
def opencat():
    
    '''open the official sz cat'''
    data = None
    coordinates = list()
    try:
        with fits.open(CATALOG) as fits_data:
            data = fits_data[1].columns
            datas = fits_data[1].data
            
    except IOError:
        datas = None
    for item in datas:
        coordinates.append([item[2], item[3], item[14]])
    return coordinates
        
def compare(txtlon, txtla, catlon, catla):
    
    '''match clusters'''
    cat1 = SkyCoord(ra=txtlon*u.degree, dec=txtla*u.degree)
    cat2 = SkyCoord(ra=catlon*u.degree, dec=catla*u.degree)
    
    idx, d2d, d3d = match_coordinates_sky(cat1, cat2)
    
    matches = cat2[idx]  
    
    dra = np.array((matches.ra - cat1.ra).arcmin)
    ddec = np.array((matches.dec - cat1.dec).arcmin)
    erreurtot = np.sqrt(np.absolute(dra)**2 + np.absolute(ddec)**2)
    
    
    faux = erreurtot > 12
    true = erreurtot <= 12
    for i, item in enumerate(faux):
        if item ==1:
            print('indice faux :')
            print(i)
        
    fauxcl = cat1[faux]
    dratr = dra[true]
    ddectr = ddec[true]
    erreur = np.sqrt((dratr.sum())**2 + (ddectr.sum())**2)/len(ddectr)
    fauxcat = matches[faux]
    print('coordonnées des faux clusters dans le catalogue officiel')
    print(fauxcat)
    print('coordonnées des faux clusters dans notre catalogue')
    print(fauxcl)
    print('erreur totale des faux clusters')
    print(erreurtot[faux])
    print('erreur totale moyenne  des vrais clusters')
    print(erreur)
def main():
    
    clusterfind = opentxt()
    clustercat = opencat()
    
    txtlon = np.array(clusterfind)[:,0]
    txtla = np.array(clusterfind)[:,1]
    catla = np.array(clustercat)[:,1]
    catlon = (np.array(clustercat)[:,0])
    compare(txtlon, txtla, catlon, catla)
    
    
if __name__ == '__main__':
    main()